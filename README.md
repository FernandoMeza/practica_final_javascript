# Javascript

Práctica final curso Javascript.

## Autor

*Meza Ortega Fernando*

## Descripción

En esta práctica se modificó una parte de una página web para interactuar con la API de "themoviedb", el objetivo es que se muestre el resultado de la busqueda de una frase que el usuario ingrese llamando a la API. 

## Solución de la práctica

Para utilizar el API se tuvo que corregir el link porque el token parece que era incorrecto, al utilizar el token en el link del archivo "index.js" responde correctamente. Me basé en el código del archivo "catalogo.js", sólo agregué la fecha de salida (la que no se mostrará en caso de que no exista), utilicé un gif distinto para la espera de la respuesta y modifiqué la palabra del botón "agregar" por "buscar".